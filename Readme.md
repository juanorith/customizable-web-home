
[Versión en español de este archivo](_locales/es/Readme.md)

# Customizable web home

Firefox extension that changes the home page and the new tab page to a customizable web page

This extension asks for the following permissions:
- `storage` : to save the customizable data of the page
- `unlimitedStorage` : to not limit the amount of data that the extension can save. In the extension is possible to add images from files to the customizable page, to not limit the sizes these files this unlimited storage permission is needed

The source code of the extension can be found in [this gitlab repository](https://gitlab.com/juanorith/customizable-web-home), if you want to contribute to this project feel free to do it!

This software is licensed under the GNU General Public License version 2 [see the license file for more detail](License.txt)

The images packed with this software are originals and licensed under Creative Commons Attribution 4.0 International (CC BY 4.0) [see the images license file for more detail](images/License_images.txt)
